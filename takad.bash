#!/usr/bin/env bash
# Auto-generated file with gildor478-build-tools/takad/setup.bash, don't edit.
readonly HIZIVADUR_REPOSITORY_EXTERNAL="stalian+https://embannd-europe-west1-wrbom5nada-ew.a.run.app/gitlab/gildor478/hizivadur-repository-external?version=1.7.0"
readonly HIZIVADUR_REPOSITORY_INHOUSESOFT="stalian+https://embannd-europe-west1-wrbom5nada-ew.a.run.app/gitlab/gildor478/hizivadur-repository-inhousesoft?version=1.27.0"
readonly DISPAKAN_VERSION=1.10.0
readonly GILDOR478_ENV_GO_VERSION=20230113.0.0
readonly HIZIVADUR_VERSION=2.2.0
readonly KEEPACHANGELOG_VERSION=1.1.0
readonly SEKRED_VERSION=0.2.3
readonly TAKAD_VERSION=1.8.3
