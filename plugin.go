package savete

import (
	"context"

	"gitlab.com/gildor478/savete/internal/archiver"
	"gitlab.com/gildor478/savete/internal/userdata"
)

type Plugin interface {
	Run(ctx context.Context, ud *userdata.UserData, ar archiver.Archiver) error
	Name() string
}
