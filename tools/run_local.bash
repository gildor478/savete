#!/usr/bin/env bash

set -e

. run-local-env.sh || exit 1

: "${GOOGLE_APPLICATION_CREDENTIALS:?not set}"

export GOOGLE_APPLICATION_CREDENTIALS

go run ./cmd/saveted --verbose \
  --grpckit_default_url "grpc://api.mererezh.le-gall.net:443" \
  --accounts_filename "$(readlink -f ./tools/test_accounts.json)"