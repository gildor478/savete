#!/usr/bin/env bash

set -e

: ${BUILD_TOOLS:=tools/gildor478-build-tools}

if [ -n "${CI_JOB_TOKEN}" ] ; then
  if ! [ -e "${HOME}/.netrc" ] ; then
    cat  > "${HOME}/.netrc" <<EOF
machine gitlab.com
login gitlab-ci-token
password ${CI_JOB_TOKEN}
EOF
  else
    echo "I: ${HOME}/.netrc already exists, not overwriting it." >&2
  fi
fi

if [ -e .gitmodules ] ; then
  git ${BUILD_TOOLS_GIT_OPTIONS} submodule sync --quiet --recursive
  git ${BUILD_TOOLS_GIT_OPTIONS} submodule update --quiet --init --recursive
fi

if [ -e .gitattributes ] ; then
  pushd $HOME > /dev/null
  git lfs install
  popd > /dev/null
fi
