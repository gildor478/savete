HIZIVADUR_PACKAGES =

default: test

.PHONY: default

-include env.mk
-include integration-test.mk
include tools/gildor478-build-tools/hizivadur.mk
include tools/gildor478-build-tools/dispakan.mk
include tools/gildor478-build-tools/gitlab-ci.mk
include tools/gildor478-build-tools/golang.mk
include tools/gildor478-build-tools/test.mk
include tools/gildor478-build-tools/takad.mk

$(eval $(call executable_rule,./cmd/savetec))
$(eval $(call executable_rule,./cmd/saveted))
