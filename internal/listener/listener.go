package listener

import (
	"fmt"

	"gitlab.com/gildor478/combinerrors"
	"gitlab.com/gildor478/diazez-go/diazez/diazezconf"
)

type Listener struct {
	port        int
	bindAddress string
}

func New(opts ...Option) *Listener {
	l := &Listener{
		port:        9000,
		bindAddress: "localhost",
	}
	for _, f := range opts {
		f(l)
	}
	return l
}

func (c *Listener) MergeConf(conf *diazezconf.Conf) error {
	conf.MustAddVar(diazezconf.NewIntVar(
		"port",
		&c.port,
		diazezconf.Usage(`Port number to use`),
		diazezconf.AddValidator(combinerrors.StrictlyPositiveInt("port number must be positive", &c.port))))
	conf.MustAddVar(diazezconf.NewStringVar(
		"bind_address",
		&c.bindAddress,
		diazezconf.Usage(`Bind address to use`)))
	return nil
}

func (l *Listener) Addr() string {
	return fmt.Sprintf("%s:%d", l.bindAddress, l.port)
}

type Option func(l *Listener)

func WithDefaultPort(i int) Option {
	return func(l *Listener) {
		l.port = i
	}
}
