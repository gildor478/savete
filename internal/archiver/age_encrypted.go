package archiver

import (
	"context"
	"fmt"
	"io"

	"filippo.io/age"
	"gitlab.com/gildor478/closers"
)

var _ Archiver = (*AgeEncrypted)(nil)

type AgeEncrypted struct {
	recipients []age.Recipient
	archiver   Archiver
}

func NewAgeEncrypted(ar Archiver, recipients []age.Recipient) *AgeEncrypted {
	return &AgeEncrypted{
		recipients: recipients,
		archiver:   ar,
	}
}

func (a *AgeEncrypted) name(n string) string {
	return n + ".age"
}

func (a *AgeEncrypted) CreateArchive(ctx context.Context, name string, size uint64) (io.WriteCloser, string, int, error) {
	clsrs := closers.New()
	defer clsrs.CloseWithPanic()
	notDone := true

	fd, fn, bufferSize, err := a.archiver.CreateArchive(ctx, a.name(name), a.predictSize(size))
	if err != nil {
		return nil, "", 0, err
	}
	clsrs.RegisterCloser(fd, closers.CloseOnlyIf(&notDone))

	ad, err := age.Encrypt(fd, a.recipients...)
	if err != nil {
		return nil, "", 0, fmt.Errorf("%w when open age encrypted channel", err)
	}
	clsrs.RegisterCloser(ad, closers.CloseOnlyIf(&notDone))

	notDone = false
	w := &writerClosers{
		writer:  ad,
		closers: closers.New(),
	}
	w.closers.RegisterCloser(fd)
	w.closers.RegisterCloser(ad)
	return w, fn, bufferSize, nil
}

func (a *AgeEncrypted) ArchiveExists(ctx context.Context, name string) (bool, error) {
	return a.archiver.ArchiveExists(ctx, a.name(name))
}

func (a *AgeEncrypted) predictSize(size uint64) uint64 {
	// TODO
	return size
}
