package archiver

import (
	"io"

	"gitlab.com/gildor478/closers"
)

type writerClosers struct {
	writer  io.Writer
	closers *closers.Closers
}

func (w *writerClosers) Write(b []byte) (int, error) {
	return w.writer.Write(b)
}

func (w *writerClosers) Close() error {
	return w.closers.Close()
}
