package archiver

import (
	"context"
	"io"
)

type Archiver interface {
	// CreateArchive creates an archive specific to a user. It typically includes encryption for the specific user
	// or a specific directory to store data for a given user. It also returns the maximum buffer size for writing and
	// the name of the target file.
	CreateArchive(ctx context.Context, name string, size uint64) (io.WriteCloser, string, int, error)

	// ArchiveExists tests the existence of a specific archive. It typically includes any specific modification to
	// the name like CreateArchive is doing.
	ArchiveExists(ctx context.Context, name string) (bool, error)
}
