package archiver

import (
	"context"
	"fmt"
	"io"

	"gitlab.com/gildor478/blindsyncc/blindsync"

	bspb "gitlab.com/gildor478/gildor478-protos/go/blindsync"
)

var _ Archiver = (*Blindsync)(nil)

type Blindsync struct {
	client bspb.BlindsyncClient
	prefix string
}

func NewBlindsync(c bspb.BlindsyncClient, prefix string) *Blindsync {
	return &Blindsync{
		client: c,
		prefix: prefix,
	}
}

func (b *Blindsync) name(n string) string {
	return b.prefix + n
}

func (b *Blindsync) CreateArchive(ctx context.Context, name string, size uint64) (io.WriteCloser, string, int, error) {
	n := b.name(name)
	fd, buffer, err := blindsync.CreateWriter(
		ctx,
		b.client,
		blindsync.WriterDisplaySize(size),
		blindsync.WriterDisplayName(n),
	)
	if err != nil {
		return nil, "", 0, err
	}
	return fd, "blindsync://" + n, len(buffer), nil
}

func (b *Blindsync) ArchiveExists(ctx context.Context, name string) (bool, error) {
	n := b.name(name)
	req := &bspb.ListFilesRequest{}
	resp, err := b.client.ListFiles(ctx, req)
	if err != nil {
		return false, fmt.Errorf("%w when listing blindsync files", err)
	}
	for _, f := range resp.Files {
		if f.DisplayFilename == n {
			return true, nil
		}
	}
	return false, nil
}
