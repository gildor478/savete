package archiver

import (
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/gildor478/fileutils"
	"gopkg.in/src-d/go-billy.v4"
)

var _ Archiver = (*File)(nil)

type File struct {
	directory  string
	filesystem billy.Filesystem
}

func NewFile(filesystem billy.Filesystem, dn string) *File {
	return &File{
		directory:  dn,
		filesystem: filesystem,
	}
}

func (f *File) filename(n string) string {
	return filepath.Join(f.directory, n)
}

func (f *File) CreateArchive(_ context.Context, name string, _ uint64) (io.WriteCloser, string, int, error) {
	if err := os.MkdirAll(f.directory, 0700); err != nil {
		return nil, "", 0, fmt.Errorf("%w when creating directory %q", err, f.directory)
	}
	fn := f.filename(name)
	fd, err := os.OpenFile(fn, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0600)
	if err != nil {
		return nil, "", 0, fmt.Errorf("%w when opening file %q", err, fn)
	}
	return fd, "file://" + fn, 32 * 1024, nil
}

func (f *File) ArchiveExists(ctx context.Context, name string) (bool, error) {
	return fileutils.Test(ctx, f.filesystem, f.filename(name), fileutils.TestExists)
}
