package userdata

import (
	"context"
	"encoding/json"
	"fmt"

	"filippo.io/age"
	"gitlab.com/gildor478/fileutils"
	"gopkg.in/src-d/go-billy.v4"
)

type UserData struct {
	NoterID          int64
	Email            string
	X25519Recipients []string
	Recipients       []age.Recipient
}

func Load(ctx context.Context, filesystem billy.Filesystem, fn string) ([]*UserData, error) {
	b, err := fileutils.Cat(ctx, filesystem, fn)
	if err != nil {
		return nil, fmt.Errorf("%w when reading data from file %q", err, fn)
	}
	var l []*UserData
	if err = json.Unmarshal(b, &l); err != nil {
		return nil, fmt.Errorf("%w when decoding data from file %q", err, fn)
	}

	for _, ud := range l {
		l := make([]age.Recipient, len(ud.X25519Recipients))
		for i, r := range ud.X25519Recipients {
			var err error
			l[i], err = age.ParseX25519Recipient(r)
			if err != nil {
				return nil, fmt.Errorf("%w when decoding AGE x25519 recipient %q", err, r)
			}
		}
		ud.Recipients = l
	}
	return l, nil
}
