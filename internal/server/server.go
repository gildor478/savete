package server

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"gitlab.com/gildor478/diazez-go/diazez/diazezconf"
	"gitlab.com/gildor478/diazez-go/diazez/diazezfuncbundle"
	"gitlab.com/gildor478/diazez-go/diazez/diazezlog"
	"gitlab.com/gildor478/savete"
	"gitlab.com/gildor478/savete/internal/archiver"
	"gitlab.com/gildor478/savete/internal/listener"
	"gitlab.com/gildor478/savete/internal/userdata"
	"gopkg.in/src-d/go-billy.v4"

	bspb "gitlab.com/gildor478/gildor478-protos/go/blindsync"
)

type runPluginStatus struct {
	userData   *userdata.UserData
	pluginName string
}

type Server struct {
	listener         *listener.Listener
	accountsFilename string
	plugins          []savete.Plugin
	filesystem       billy.Filesystem
	localDirectory   string
	blindsyncClient  bspb.BlindsyncClient
	storageType      StorageType

	runPluginStatusMtx  sync.Mutex
	runPluginStatusList []runPluginStatus
}

func New(filesystem billy.Filesystem, blindsyncClient bspb.BlindsyncClient, plugins []savete.Plugin) *Server {
	l := make([]savete.Plugin, len(plugins))
	copy(l, plugins)
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	return &Server{
		listener:        listener.New(listener.WithDefaultPort(8080)),
		plugins:         l,
		filesystem:      filesystem,
		blindsyncClient: blindsyncClient,
		localDirectory:  wd,
		storageType:     StorageBlindsync,
	}
}

func (s *Server) MergeConf(conf *diazezconf.Conf) error {
	conf.Merge(s.listener)
	conf.MustAddVar(diazezconf.NewStringVar(
		"accounts_filename",
		&s.accountsFilename))
	conf.MustAddVar(diazezconf.NewEnumVar(
		"storage_type",
		&s.storageType,
		AllStorageTypes))
	conf.MustAddVar(diazezconf.NewStringVar(
		"local_directory",
		&s.localDirectory))
	return nil
}

func (s *Server) archiver(pluginName string, ud *userdata.UserData) archiver.Archiver {
	var baseArchiver archiver.Archiver
	switch s.storageType {
	case StorageBlindsync:
		baseArchiver = archiver.NewBlindsync(s.blindsyncClient, filepath.Join(ud.Email, pluginName)+"/")

	case StorageLocal:
		baseArchiver = archiver.NewFile(s.filesystem, filepath.Join(s.localDirectory, ud.Email, pluginName))
	}
	return archiver.NewAgeEncrypted(baseArchiver, ud.Recipients)
}

func (s *Server) runPlugins(ctx context.Context) error {
	logger := diazezlog.FromContext(ctx)

	accounts, err := userdata.Load(ctx, s.filesystem, s.accountsFilename)
	if err != nil {
		return fmt.Errorf("%w when loading account data", err)
	}

	for _, ud := range accounts {
		for _, p := range s.plugins {
			s.runPluginStatusMtx.Lock()
			s.runPluginStatusList = append(s.runPluginStatusList, runPluginStatus{
				userData:   ud,
				pluginName: p.Name(),
			})
			s.runPluginStatusMtx.Unlock()
			ar := s.archiver(p.Name(), ud)
			if err := p.Run(ctx, ud, ar); err != nil {
				return fmt.Errorf("%w when running plugin %q", err, p.Name())
			}
		}
	}

	logger.Infof("Successfully processed %d plugins across %d account.", len(s.plugins), len(accounts))

	return nil
}

func (s *Server) ListenAndServe(ctx context.Context) error {
	handler := http.NewServeMux()
	handler.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.RequestURI != "/" {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		s.runPluginStatusMtx.Lock()
		defer s.runPluginStatusMtx.Unlock()
		if _, err := fmt.Fprintf(w, "%d plugins/users processed.\n", len(s.runPluginStatusList)); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		for _, s := range s.runPluginStatusList {
			if _, err := fmt.Fprintf(w, "- %s/%s\n", s.userData.Email, s.pluginName); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
	})
	srv := &http.Server{
		Addr:         s.listener.Addr(),
		Handler:      handler,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		BaseContext: func(n net.Listener) context.Context {
			return ctx
		},
	}
	return diazezfuncbundle.Run(ctx,
		diazezfuncbundle.Func(func(ctx context.Context) error {
			if err := srv.ListenAndServe(); err != nil {
				if errors.Is(err, http.ErrServerClosed) {
					return nil
				}
				return fmt.Errorf("%w when running savete daemon on %s", err, s.listener.Addr())
			}
			return nil
		}, diazezfuncbundle.CancelIfExit()),
		diazezfuncbundle.Func(s.runPlugins),
		diazezfuncbundle.Func(func(ctx context.Context) error {
			logger := diazezlog.FromContext(ctx)
			<-ctx.Done()
			logger.Infof("Shutting down savete server on %s.", s.listener.Addr())
			return srv.Close()
		}))
}
