package server

type StorageType string

const (
	StorageLocal     StorageType = "local"
	StorageBlindsync StorageType = "blindsync"
)

var AllStorageTypes = []StorageType{
	StorageLocal, StorageBlindsync,
}

func (st StorageType) String() string {
	return string(st)
}
