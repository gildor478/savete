package googletakeout

import (
	"context"
	"fmt"
	"io"
	"strings"

	"gitlab.com/gildor478/closers"
	"gitlab.com/gildor478/diazez-go/diazez/diazezconf"
	"gitlab.com/gildor478/diazez-go/diazez/diazezlog"
	"gitlab.com/gildor478/savete/internal/archiver"
	"gitlab.com/gildor478/savete/internal/userdata"
	"google.golang.org/api/drive/v3"
)

type Plugin struct {
}

func New() *Plugin {
	return &Plugin{}
}

func (p *Plugin) MergeConf(_ *diazezconf.Conf) error {
	return nil
}

type file struct {
	name      string
	driveId   string
	sizeBytes int64
}

func (f *file) String() string {
	return fmt.Sprintf("%s (id: %s)", f.name, f.driveId)
}

func (p *Plugin) Name() string {
	return "google-takeout"
}

func (p *Plugin) Run(ctx context.Context, ud *userdata.UserData, ar archiver.Archiver) error {
	logger := diazezlog.FromContext(ctx)

	var files []*file

	c, err := drive.NewService(ctx)
	if err != nil {
		return err
	}
	l, err := c.Files.List().Context(ctx).
		Fields("nextPageToken", "files(id, name, owners, size)").
		Q(fmt.Sprintf("%q in owners and name contains 'takeout_' and mimeType = 'application/x-zip'", ud.Email)).
		Do()
	if err != nil {
		return fmt.Errorf("%w when searching for files on Drive", err)
	}
	if len(l.Files) == 0 {
		logger.Info("No Google Takeout files found on Drive.")
	} else {
		for _, f := range l.Files {
			ff := &file{
				name:      f.Name,
				driveId:   f.Id,
				sizeBytes: f.Size,
			}
			if len(f.Owners) != 1 {
				logger.Warningf("Google File %s has %d owners, it should have 1.", ff, len(f.Owners))
				continue
			}
			if owner := f.Owners[0].EmailAddress; owner != ud.Email {
				logger.Warningf("Google File %s has %s owner, it should be owned by %s.", ff, owner, ud.Email)
				continue
			}
			if !strings.HasPrefix(f.Name, "takeout-") || !strings.HasSuffix(f.Name, ".zip") {
				logger.Warningf("Google File %s should start with takeout and ends with .zip.", ff)
				continue
			}
			logger.Infof("Found Google Takeout file %s.", ff)
			files = append(files, ff)
		}
	}

	clsrs := closers.New()
	defer clsrs.CloseWithPanic()

	if err != nil {
		return fmt.Errorf("%w when determining working directory", err)
	}
	for _, f := range files {
		if ok, err := ar.ArchiveExists(ctx, f.name); err != nil {
			return err
		} else if ok {
			logger.Infof("Google File %s already exists in archive.", f)
			continue
		}

		r, err := c.Files.Get(f.driveId).Context(ctx).AcknowledgeAbuse(true).Download()
		if err != nil {
			return fmt.Errorf("%w when downloading Google File %q (id: %q)", err, f.name, f.driveId)
		}
		closeBody := clsrs.RegisterCloser(r.Body)

		fd, fn, bufferSize, err := ar.CreateArchive(ctx, f.name, uint64(f.sizeBytes))
		if err != nil {
			return fmt.Errorf("%w when opening archive %q", err, f.name)
		}
		closeArchive := clsrs.RegisterCloser(fd)

		logger.Infof("Copying Google File %s to archive %q.", f, fn)

		buffer := make([]byte, bufferSize)

		if _, err := io.CopyBuffer(fd, r.Body, buffer); err != nil {
			return fmt.Errorf("%w when copying Google File %s to archive", err, f)
		}

		if err := closeArchive(); err != nil {
			return fmt.Errorf("%w when closing archive", err)
		}

		if err := closeBody(); err != nil {
			return fmt.Errorf("%w when closing Google File %s", err, f)
		}
	}

	return clsrs.Close()
}
