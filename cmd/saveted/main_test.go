package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gildor478/grpckit-go/v2/grpckit/platform/gcp/gcpdefaults/gcpdefaultstesting"
)

func TestRun(t *testing.T) {
	require.NoError(t, gcpdefaultstesting.TestSetUpFn(appSetUp))
}
