package main

import (
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/gildor478/blindsyncc/blindsync"
	"gitlab.com/gildor478/diazez-go/diazez/diazezapp"
	"gitlab.com/gildor478/diazez-go/diazez/diazezsecret/secretmanager"
	"gitlab.com/gildor478/fileutils"
	"gitlab.com/gildor478/grpckit-go/v2/grpckit/connection"
	"gitlab.com/gildor478/grpckit-go/v2/grpckit/platform/gcp/gcpdefaults"
	"gitlab.com/gildor478/noter/noterauthclient"
	"gitlab.com/gildor478/noter/noterauthclient/storagexdg"
	"gitlab.com/gildor478/savete"
	"gitlab.com/gildor478/savete/internal/plugins/googletakeout"
	"gitlab.com/gildor478/savete/internal/server"

	bspb "gitlab.com/gildor478/gildor478-protos/go/blindsync"
	napb "gitlab.com/gildor478/gildor478-protos/go/noter/noterauth"
)

const appName = "noterd"

func appSetUp(_ *gcpdefaults.Defaults, app *diazezapp.Application) (*diazezapp.Command, error) {
	gc := connection.NewGlobalConf()
	app.Register(gc)

	pgt := googletakeout.New()
	app.Register(pgt)

	fs := fileutils.DefaultFilesystem()
	app.Register(fs)

	noterAuthClientProvider := napb.NewNoterAuthClientWithConf(gc)
	app.Register(noterAuthClientProvider, diazezapp.ExpectCloser())

	sm := secretmanager.New()
	app.Register(sm)

	noterAuthClient, err := noterauthclient.New(
		noterAuthClientProvider,
		appName,
		[]string{blindsync.UserRole},
		sm,
		noterauthclient.UseStorage(storagexdg.New))
	if err != nil {
		return nil, errors.Wrap(err, "in noterauthclient.New()")
	}
	app.Register(noterAuthClient, diazezapp.ExpectCloser())

	bsc := bspb.NewBlindsyncClientWithConf(gc, connection.Creds(noterAuthClient))
	app.Register(bsc, diazezapp.ExpectCloser())

	s := server.New(fs, bsc, []savete.Plugin{pgt})
	app.Register(s)

	return app.Command(&cobra.Command{
		Use:   appName,
		Short: "savete daemon",
		Args:  cobra.NoArgs,
	}, s.ListenAndServe, diazezapp.AllRegisteredInterfaces), nil
}

func main() {
	gcpdefaults.Run(appSetUp, appName)
}
